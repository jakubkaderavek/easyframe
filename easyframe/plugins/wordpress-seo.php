<?php

// move WP SEO box down

add_filter('wpseo_metabox_prio', function () {
    return 'low';
});
