<?php
// loop for pages
?>

<?php
if (have_posts()) {
    while (have_posts()) {
        the_post();
        ?>
        <h1><?php the_title(); ?></h1>
        <?php the_content(); ?>
        <?php edit_post_link(); ?>
        <?php comments_template('', true); ?>
        <?php
    }
}