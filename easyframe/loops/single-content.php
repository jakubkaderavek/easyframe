<?php
// loop for single
?>

<?php
if (have_posts()) {
    while (have_posts()) {
        the_post();
        ?>
        <h1><?php the_title(); ?></h1>
        <p><em><?php echo get_the_date(); ?> | <?php echo get_the_author(); ?></em></p>
        <?php the_tags( '<p>' . __('Štítky', EF_THEME) . ' ', ', ', '</p>' ); ?>         
        <?php next_posts_link(); ?>
        <?php the_content(); ?>
        <?php wp_link_pages(); ?>
        <?php edit_post_link(); ?>
        <?php comments_template('', true); ?>
        <?php
    }
} ?>