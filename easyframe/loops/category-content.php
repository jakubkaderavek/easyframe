<?php
// loop for category
?>

<?php
if (have_posts()) {
    while (have_posts()) {
        the_post();
        ?>
        <article>
            <?php
            if (is_author()) {
                ?>
                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            <?php } else { ?>
                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <?php } ?>
            <p><em><?php echo get_the_date(); ?> | <?php echo get_the_author(); ?></em></p>
            <?php the_post_thumbnail('medium'); ?>
            <p><?php
            if (has_excerpt()) {
                the_excerpt();
            } else {
                echo wp_trim_words(strip_shortcodes(get_the_content()), 54);
            }
            ?>...<a href="<?php the_permalink(); ?>"><?php _e('více', EF_THEME); ?></a></p>
        </article>
        <?php
    }
    global $wp_query;
    if ($wp_query->max_num_pages > 1) {
        ?>
        <div class="pagination">
            <div class="prev"><?php next_posts_link(__('&lt;&lt; Starší příspěvky', EF_THEME)); ?></div>
            <div class="next"><?php previous_posts_link(__('Novější příspěvky &gt;&gt;', EF_THEME)); ?></div>
        </div><!-- .pagination -->
        <?php
    }
} else {
    if (!is_search()) {
        ?>
        <p><?php _e('Omlouváme se, ale v rubrice není žádný příspěvek.', EF_THEME); ?></p>    
        <?php
    }
} ?>