<?php
// comments
?>

<div id="comments" class="comments">
    <?php
    if (post_password_required()) { ?>
            <p><?php _e('Tento příspěvek je chráněn heslem. Vložte heslo pro zobrazení komentářů.', EF_THEME); ?></p>
        </div>
    <?php
    return;
    }
if (comments_open()) {?>
    <h2><?php _e('Přidejte se do diskuze!', EF_THEME); ?></h2>
<?php
}
if (have_comments()) { ?>
    <h3><?php printf(_n('Jedna reakce: %2$s', '%1$s Odpovědi na %2$s', get_comments_number(), EF_THEME), number_format_i18n(get_comments_number()), '<em>' . get_the_title() . '</em>'); ?></h3>
    <?php
    if (get_comment_pages_count() > 1 && get_option('page_comments')) {
        ?>
        <div class="pagination">
            <?php previous_comments_link(__('Starší komentáře', EF_THEME)); ?>
            <?php next_comments_link(__('Novější komentáře', EF_THEME)); ?>
        </div>
    <?php } ?>
    <?php wp_list_comments(array('style' => 'div')); ?>
    <?php
    if (get_comment_pages_count() > 1 && get_option('page_comments')) {
        ?>
        <div class="pagination">
            <?php previous_comments_link(__('Starší komentáře', EF_THEME)); ?>
            <?php next_comments_link(__('Novější komentáře', EF_THEME)); ?>
        </div>
    <?php } ?>
<?php } ?>
<?php if (comments_open()) {
   comment_form();
}
?>
</div>

<?php
