<?php
// search form
?>

<form role="search" method="get" id="searchform" action="<?php echo home_url('/'); ?>">
   	<label for="s"><?php _e('Vyhledávání:', EF_THEME); ?></label>
    <input type="text" value="<?php echo get_search_query(); ?>" name="s" id="s">
    <button><?php _e('Hledat', EF_THEME); ?></button>
</form>
