jQuery(document).ready(function($) {
    var fancybox_settings = {
        helpers: {
            overlay: {
                locked: false
            }
        },
        tpl: {
            prev     : '<a title="' + ef_string.fancybox_prev + '" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>',
            next     : '<a title="' + ef_string.fancybox_next + '" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
            closeBtn : '<a title="' + ef_string.fancybox_close + '" class="fancybox-item fancybox-close" href="javascript:;"></a>',
            error    : '<p class="fancybox-error">' + ef_string.fancybox_error + '</p>',
        }
    };
    $('img').parent('a').fancybox(fancybox_settings);
    $('picture').parent('a').fancybox(fancybox_settings);
    $('.fancybox').fancybox(fancybox_settings);
 });
