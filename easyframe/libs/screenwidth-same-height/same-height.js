jQuery.fn.sameheight = function(options) {
    var that = this;
    var settings = jQuery.extend({
        minwidth: 0,
        maxwidth: 10,
    }, options);

    function ef_same_height() {
        setTimeout(function() {
            if (jQuery.screenwidth() >= settings.minwidth && jQuery.screenwidth() <= settings.maxwidth) {
                var max_height = 0;
                jQuery(that).each(function() {
                    jQuery(this).css('height', 'auto');
                });
                jQuery(that).each(function() {
                    if (jQuery(this).height() > max_height) {
                        max_height = jQuery(this).height();
                    }
                });
                jQuery(that).each(function() {
                    jQuery(this).css('height', max_height + 'px');
                });
            } else {
                jQuery(that).each(function() {
                    jQuery(this).css('height', 'auto');
                });
            }
        }, 50);
    }
    ef_same_height();
    jQuery(window).resize(ef_same_height);
};