<?php
get_header();

if (have_posts()) {
    the_post();
    ?>
    <h1><?php echo get_the_author(); ?></h1>
    <?php if (get_the_author_meta('description')) { ?>
        <div class="clearfix">
            <h2><?php _e('O autorovi', EF_THEME); ?></h2>
            <p><?php the_author_meta('description'); ?></p>
        </div>
    <?php } ?>
    <h2><?php _e('Příspěvky autora', EF_THEME); ?></h2>
    <?php
    rewind_posts();
    get_template_part('loops/category-content');
}

get_footer();