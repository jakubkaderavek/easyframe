<?php
get_header();
?>

<h1><?php single_tag_title(); ?></h1>
<div class="clearfix"><?php echo tag_description(); ?></div>
<?php get_template_part('loops/category-content'); ?>

<?php get_footer();