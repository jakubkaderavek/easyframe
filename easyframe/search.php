<?php
get_header();
?>

<?php
if (have_posts()) {
    ?>
    <h1><?php printf(__('Výsledky vyhledávání pro: %s', EF_THEME), '<span>' . get_search_query() . '</span>'); ?></h1>
    <?php get_template_part('loops/category-content'); ?>
    <?php
} else {
    ?>
    <h1><?php _e('Nenalezeno', EF_THEME); ?></h1>
    <p><?php _e('Omlouváme se, ale žádná stránka neexistuje dle vyhledávacích kritérií. Zkuste jí najít pomocí jiných klíčových slov.', EF_THEME); ?></p>
    <?php get_search_form(); ?>
<?php } ?>

<?php get_footer();