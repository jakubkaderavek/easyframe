<?php
// theme settings

// content width (must be before images sizes)

if(!isset($content_width)) {
    $content_width = 1920;
    if(EF_RETINA) {
        $content_width *= 2;
    }

}

add_action('after_setup_theme', 'ef_theme_setup');

function ef_theme_setup()
{
    load_theme_textdomain(EF_THEME, get_template_directory() . '/languages');
    register_nav_menus(array(
        'menu' => __('Hlavní menu', EF_THEME),
    ));
    /*
    add_image_size('custom', 600, 450, true);
    add_image_size('custom_2x', 1200, 900, true);
    */

}

// delete default gallery style
add_filter('use_default_gallery_style', '__return_false');

// theme supports

add_theme_support('post-thumbnails');
add_theme_support('automatic-feed-links');
add_theme_support('title-tag');
// clean wp head

remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wp_generator');

remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_styles', 'print_emoji_styles');


add_filter( 'wp_default_scripts', 'wpj_remove_jquery_migrate' );

function wpj_remove_jquery_migrate( &$scripts){
	if(!is_admin()){
		$scripts->remove( 'jquery');
		$scripts->add( 'jquery', false, array( 'jquery-core' ), '1.2.1' );
	}
}

// frontend enqueue styles and scripts
add_action('wp_enqueue_scripts', 'ef_theme_styles_scripts');

function ef_theme_styles_scripts()
{
    // scripts
    wp_enqueue_script('jquery');

    wp_dequeue_script('wp-embed');
    wp_deregister_script('wp-embed');

    wp_enqueue_style( 'wpweekend', EF_ASSETS_URL . '/css/style' . EF_VERSION . 'min.css', array(), null );

    if(ef_is_local() && EF_DEBUG == true) {
        wp_register_script('livereload', 'http://localhost:35729/livereload.js?snipver=1', null, null, false);
        wp_enqueue_script('livereload');
    }
}

add_action('wp_footer', 'ef_register_string', 5);

function ef_register_string()
{
    $params = array(
        'ajax_url' => admin_url('admin-ajax.php'),
        'ef_nonce' => wp_create_nonce('user_nonce'),
        'template_directory' => get_template_directory_uri(),
        'fancybox_prev' => __('Předchozí', EF_THEME),
        'fancybox_next' => __('Další', EF_THEME),
        'fancybox_close' => __('Zavřít', EF_THEME),
        'fancybox_error' => __('Požadovaný obsah se nepodařilo načíst.<br>Zkuste to prosím později.', EF_THEME),
    );

    echo '<script type="text/javascript">' . "\n";
    echo '/* <![CDATA[ */' . "\n";
    echo 'var ef_string = ' . json_encode($params) . "\n";
    echo '/* ]]> */' . "\n";
    echo '</script>' . "\n";

}


// admin enqueue styles and scripts

add_action('admin_enqueue_scripts', 'ef_admin_styles_scripts');

function ef_admin_styles_scripts()
{
    if(ef_is_local() && EF_DEBUG == true) {
        wp_register_script('livereload', 'http://localhost:35729/livereload.js?snipver=1', null, null, false);
        wp_enqueue_script('livereload');
    }
}

// redirect to home after logout from admin or to previous page

add_action('wp_logout', 'ef_logout_redirect');

function ef_logout_redirect()
{
    if(strpos($_SERVER['HTTP_REFERER'], 'wp-admin') !== false) {
        wp_redirect(home_url());
    } else {
        wp_redirect($_SERVER['HTTP_REFERER']);
    }
    exit;
}

// detect if is on in dev

function ef_is_local()
{
    if(strpos(home_url(), '.loc') !== false) {
        return true;
    } else {
        return false;
    }
}

function ef_remove_spaces($output)
{
    $output = str_replace(array("\r\n", "\r"), "\n", $output);
    $lines = explode("\n", $output);
    $new_lines = array();
    foreach ($lines as $i => $line) {
        if(!empty($line))
            $new_lines[] = trim($line);
    }
    return implode($new_lines);
}

function ef_url_exists($url)
{
    if(!$fp = curl_init($url)) {
        return false;
    } else {
        return true;
    }
}

function ef_breakpoints($min = false)
{
    if($min == true) {
        return array(1 => 479, 2 => 579, 3 => 767, 4 => 991, 5 => 1169, 6 => 1399);
    } else {
        return array(1 => 480, 2 => 580, 3 => 768, 4 => 992, 5 => 1170, 6 => 1400);
    }
}
