<?php
// remove pages

add_action('admin_menu', 'ef_remove_admin_pages', 999);

function ef_remove_admin_pages()
{
    add_menu_page(__('Menu', EF_THEME), __('Menu', EF_THEME), 'manage_options', 'nav-menus.php', '', '', 35);
    remove_submenu_page('themes.php', 'nav-menus.php');

    if(ef_get_user_role() == 'web_admin') {
        remove_menu_page('tools.php');
        remove_menu_page('upload.php');
        remove_menu_page('themes.php');
        remove_menu_page('options-general.php');
        remove_menu_page('wpseo_dashboard');
        remove_menu_page('edit.php?post_type=acf-field-group');
    }
}