<?php


// settings editor

add_filter('mce_buttons', 'ef_extended_editor_mce_buttons', 0);

function ef_extended_editor_mce_buttons($buttons) {
    return array(
        'bold',
        'italic',
        'bullist',
        'numlist',
        'blockquote',
        'link',
        'unlink',
        'strikethrough',
        'spellchecker',
        'wp_fullscreen',
        'formatselect',
        'pastetext',
        'removeformat',
        'undo',
        'redo',
        'wp_more',
        'wp_help'
    );
}

add_filter('mce_buttons_2', 'ef_extended_editor_mce_buttons_2', 0);

function ef_extended_editor_mce_buttons_2($buttons) {
    return array();
}

add_filter('tiny_mce_before_init', 'ef_custom_format_settings');

function ef_custom_format_settings($init) {
    $init['block_formats'] = (string)__('Odstavec', EF_THEME) . '=p;' . __('Nadpis', EF_THEME) . ' 2=h2;' . __('Nadpis', EF_THEME) . ' 3=h3;' . __('Nadpis', EF_THEME) . ' 4=h4;' . __('Nadpis', EF_THEME) . ' 5=h5;' . __('Nadpis', EF_THEME) . ' 6=h6';
    return $init;
}
