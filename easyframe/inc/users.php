<?php

// new user role
add_action('init', 'ef_add_role');

function ef_add_role() {
  	add_role('web_admin', __('Správce webu', EF_THEME));
}

add_action( 'admin_init', 'ef_add_user_caps');

function ef_add_user_caps() {
    $role = get_role('web_admin');
  	$role->add_cap('create_users');
    $role->add_cap('delete_others_pages');
    $role->add_cap('delete_others_posts');
    $role->add_cap('delete_pages');
    $role->add_cap('delete_posts');
    $role->add_cap('delete_private_pages');
    $role->add_cap('delete_private_posts');
    $role->add_cap('delete_published_pages');
    $role->add_cap('delete_published_posts');
    $role->add_cap('delete_users');
    $role->add_cap('edit_others_pages');
    $role->add_cap('edit_others_posts');
    $role->add_cap('edit_pages');
    $role->add_cap('edit_posts');
    $role->add_cap('edit_private_posts');
    $role->add_cap('edit_private_pages');
    $role->add_cap('edit_published_pages');
    $role->add_cap('edit_published_posts');
    $role->add_cap('edit_theme_options');
    $role->add_cap('edit_users');
    $role->add_cap('list_users');
    $role->add_cap('manage_categories');
    $role->add_cap('manage_links');
    $role->add_cap('manage_options');
    $role->add_cap('moderate_comments');
    $role->add_cap('publish_pages');
    $role->add_cap('publish_posts');
    $role->add_cap('read');
    $role->add_cap('read_private_posts');
    $role->add_cap('read_private_pages');
    $role->add_cap('remove_users');
    $role->add_cap('upload_files');
}

// get user role

function ef_get_user_role() {
    global $current_user;
    $user_roles = $current_user->roles;
    $user_role = array_shift($user_roles);
    return $user_role;
}

// remove roles

add_filter('editable_roles', 'ef_remove_higher_levels');

function ef_remove_higher_levels($all_roles) {
    $user = wp_get_current_user();
    $next_level = 'level_' . ($user->user_level + 1);

    foreach ( $all_roles as $name => $role ) {
        if (isset($role['capabilities'][$next_level])) {
            unset($all_roles[$name]);
        }
    }

    return $all_roles;
}

