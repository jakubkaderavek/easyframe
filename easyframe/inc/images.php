<?php
/*
add_filter( 'image_send_to_editor', 'ef_image_send_to_editor', 10, 2 );

function ef_image_send_to_editor( $html, $attachment_id )
{
    return '[image id="' .$attachment_id . '"]';
}
*/





function ef_get_image($url = '', $alt = '', $data = array())
{
    if($url != '' && ef_url_exists($url)) {
        $output = '';
        $output .= '<img src="' . $url . '"';
        $output .= ' alt="' . $alt . '"';
        if(isset($data['class'])) {
            $output .= ' class="' . $data['class'] . '"';
        }
        if(isset($data['attr'])) {
            $output .= ' ' . $data['attr'];
        }
        $output .= '>';
        return $output;
    } else {
        return __('Buďto chybí atribut url, nebo se obrázek nenechází na dané URL', EF_THEME);
    }

}

function ef_get_image_size($size)
{
    global $_wp_additional_image_sizes;
    $info = array();
    if(in_array($size, array('thumbnail', 'medium', 'medium_large', 'large'))) {
        $info['1'] = get_option($size . '_size_w');
        $info['2'] = get_option($size . '_size_h');
        $info['3'] = (bool)get_option($size . '_crop');
    } else if(isset($_wp_additional_image_sizes[$size])) {
        $info = array(
            '1' => $_wp_additional_image_sizes[$size]['width'],
            '2' => $_wp_additional_image_sizes[$size]['height'],
            '3' => $_wp_additional_image_sizes[$size]['crop'],
        );
    }
    return $info;
}

function ef_get_default_img($image_id, $size, $attr = array(), $nothumb = true, $srcset = false)
{
    $alt = get_post_meta($image_id, '_wp_attachment_image_alt', true);
    if($alt == '') {
        $alt = get_the_title($image_id);
    }
    $default = wp_get_attachment_image_src($image_id, $size);
    $size = ef_get_image_size($size);
    if($default == false && $nothumb = true) {
        $default[0] = EF_ASSETS_URL . '/images/nothumb/' . $size . '.png';
    }

    $output = '';
    $src = 'src';
    if($srcset == true) {
        $src = 'srcset';
    }
    $output .= '<img '. $src . '="' . $default[0] . '"';
    $output .= ' alt="' . $alt . '"';
    foreach ($attr as $key => $value) {
        $output .= ' ' . $key . '="' . $value . '"';
    }
    /*
    if($size) {
        $output .= ' width="' . $size[1] . '"';
        $output .= ' height="' . $size[2] . '"';
    }
    */
    $output .= '>';
    return $output;
}

function ef_get_image_by_id($image_id = '', $sizes = array(0 => 'thumbnail'), $attr = array(), $nothumb = true, $attr_pic = array())
{
    if($image_id != '') {
        $output = '';
        if(count($sizes) == 1) {
            $output .= ef_get_default_img($image_id, $sizes[0], $attr, $nothumb);
        } else if(EF_PICTURE) {
            // set default size as last
            $max = max(array_keys($sizes));


            $sizes[($max + 1)] = $sizes[0];
            unset($sizes[0]);




            // sort from highter val
            krsort($sizes);


            // sizes count
            $sizes_count = count($sizes);
            $breakpoints = ef_breakpoints();
            $output .= '<picture';
            foreach ($attr_pic as $key => $value) {
                $output .= ' ' . $key . '="' . $value . '"';
            }
            $output .= '>';
            $counter = 1;
            foreach ($sizes as $key => $size) {
                $img = wp_get_attachment_image_src($image_id, $size);
                if($img == false && $nothumb == true) {
                    $img = ef_get_image_size($size);
                    $img[0] = EF_ASSETS_URL . '/images/nothumb/' . $size . '.png';
                }
                // last img
                if($counter == $sizes_count) {
                    $output .= ef_get_default_img($image_id, $size, $attr, $nothumb, true);
                } else {
                    $new_key = $key - 1;
                    if(isset($breakpoints[($new_key)])) {
                        $retina = '';
                        if(EF_RETINA) {
                            $img_2x = wp_get_attachment_image_src($image_id, $size . '_2x');
                            if($img_2x) {
                                $retina = ', ' . $img_2x[0] . ' 2x';
                            }
                        }
                        $output .= '<source srcset="' . $img[0] . $retina . '"';
                        $output .= ' media="(min-width: ' . $breakpoints[($new_key)] . 'px)"';
                        $output .= '>';
                    }
                }
                $counter++;
            }
            $output .= '</picture>';
        }
        return $output;
    } else {
        return __('ID obrázku nevyplněno', EF_THEME);
    }


}