<?php

// settings

add_action('init', 'ef_register_settings', 5);

function ef_register_settings() {
    $add = __('Přidat nastavení šablony', EF_THEME);
    register_post_type('nastaveni', array(
        'labels' => array(
            'name' => __('Nastavení šablony', EF_THEME),
            'singular_name' => __('Nastavení šablony', EF_THEME),
            'name_admin_bar' => $add,
            'add_new' => $add,
            'add_new_item' => $add,
            'edit_item' => __('Editovat nastavení šablony', EF_THEME),
            'new_item' => __('Nové nastavení šablony', EF_THEME),
            'view_item' => __('Zobrazit nastavení šablony', EF_THEME),
            'search_items' => __('Hledat v nastaveních šablon', EF_THEME),
            'not_found' => __('Žádné nastavení šablony nenalezeno', EF_THEME),
            'not_found_in_trash' => __('Žádné nastavení šablony nenalezeno v koši', EF_THEME),
        ),
        'show_ui' => true,
        'supports' => array('title', 'revisions'),
    ));
}