<?php
// theme settings

// typo

function ef_dummy_typo() { ?>
    <h1>h1 test Quisque at ligula et lectus mattis feugiat id quis nibh. Curabitur lobortis consectetur purus non aliquam.</h1>
    <h2>h2 test Quisque at ligula et lectus mattis feugiat id quis nibh. Curabitur lobortis consectetur purus non aliquam.</h2>
    <h3>h3 test Quisque at ligula et lectus mattis feugiat id quis nibh. Curabitur lobortis consectetur purus non aliquam.</h3>
    <h4>h4 test Quisque at ligula et lectus mattis feugiat id quis nibh. Curabitur lobortis consectetur purus non aliquam.</h4>
    <h5>h5 test Quisque at ligula et lectus mattis feugiat id quis nibh. Curabitur lobortis consectetur purus non aliquam.</h5>
    <h6>h6 test Quisque at ligula et lectus mattis feugiat id quis nibh. Curabitur lobortis consectetur purus non aliquam.</h6>
    <p><strong>I am text block. <em>Click edit button</em> to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</strong></p>
    <p>I am text block. <em>Click edit button</em> to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>

    <p><del>I am text block.</del></p>
    <ul>
    <li>Phasellus vitae felis tristique elit fringilla tempor.</li>
    <li>Ut adipiscing neque venenatis, porttitor urna nec, suscipit libero.</li>
    <li>Donec a orci ac arcu dictum scelerisque.
        <ul>
            <li>Phasellus vitae felis tristique elit fringilla tempor.</li>
            <li>Ut adipiscing neque venenatis, porttitor urna nec, suscipit libero.</li>
            <li>Donec a orci ac arcu dictum scelerisque.</li>
            <li>Nam nec nulla nec lacus varius varius sed sit amet nulla.</li>
            <li>Nunc porttitor sem sit amet tortor porttitor vehicula.</li>
            <li>Phasellus consectetur elit eget magna malesuada egestas.</li>
        </ul>
    </li>
    <li>Nam nec nulla nec lacus varius varius sed sit amet nulla.
        <ol>
            <li>Phasellus vitae felis tristique elit fringilla tempor.</li>
            <li>Ut adipiscing neque venenatis, porttitor urna nec, suscipit libero.</li>
            <li>Donec a orci ac arcu dictum scelerisque.</li>
            <li>Nam nec nulla nec lacus varius varius sed sit amet nulla.</li>
            <li>Nunc porttitor sem sit amet tortor porttitor vehicula.</li>
            <li>Phasellus consectetur elit eget magna malesuada egestas.</li>
        </ol>
    </li>
    <li>Nunc porttitor sem sit amet tortor porttitor vehicula.</li>
    <li>Phasellus consectetur elit eget magna malesuada egestas.</li>
    </ul>
    <ol>
    <li>Phasellus vitae felis tristique elit fringilla tempor.</li>
    <li>Ut adipiscing neque venenatis, porttitor urna nec, suscipit libero.</li>
    <li>Donec a orci ac arcu dictum scelerisque.
        <ol>
            <li>Phasellus vitae felis tristique elit fringilla tempor.</li>
            <li>Ut adipiscing neque venenatis, porttitor urna nec, suscipit libero.</li>
            <li>Donec a orci ac arcu dictum scelerisque.</li>
            <li>Nam nec nulla nec lacus varius varius sed sit amet nulla.</li>
            <li>Nunc porttitor sem sit amet tortor porttitor vehicula.</li>
            <li>Phasellus consectetur elit eget magna malesuada egestas.</li>
        </ol>
    </li>
    <li>Nam nec nulla nec lacus varius varius sed sit amet nulla.
        <ul>
            <li>Phasellus vitae felis tristique elit fringilla tempor.</li>
            <li>Ut adipiscing neque venenatis, porttitor urna nec, suscipit libero.</li>
            <li>Donec a orci ac arcu dictum scelerisque.</li>
            <li>Nam nec nulla nec lacus varius varius sed sit amet nulla.</li>
            <li>Nunc porttitor sem sit amet tortor porttitor vehicula.</li>
            <li>Phasellus consectetur elit eget magna malesuada egestas.</li>
        </ul>
    </li>
    <li>Nunc porttitor sem sit amet tortor porttitor vehicula.</li>
    <li>Phasellus consectetur elit eget magna malesuada egestas.</li>
    </ol>
    <blockquote><p>
    Proin laoreet egestas purus non cursus. Praesent in mollis tellus. Nunc vitae viverra sapien. In at fermentum mi? Sed eu mauris consectetur; commodo dolor sit amet, dapibus magna. Mauris vestibulum metus ut nisl vestibulum blandit.
    </p></blockquote>
    <p><a href='#'>Proin laoreet egestas purus non cursus.</a> Praesent in mollis tellus. Nunc vitae viverra sapien. In at fermentum mi? Sed eu mauris consectetur; commodo dolor sit amet, dapibus magna. Mauris vestibulum metus ut nisl vestibulum blandit.</p>
    <table>
        <thead>
            <tr>
                <th>Lorem Ipsum dolor sit</th>
                <th>Lorem Ipsum dolor sit</th>
                <th>Lorem Ipsum dolor sit</th>
                <th>Lorem Ipsum dolor sit</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <td>Lorem Ipsum dolor sit</td>
                <td>Lorem Ipsum dolor sit</td>
                <td>Lorem Ipsum dolor sit</td>
                <td>Lorem Ipsum dolor sit</td>
            </tr>
        </tfoot>
        <tbody>
            <?php for($i = 0; $i < 10; $i++) { ?>
                <tr>
                    <td>Lorem Ipsum dolor sit</td>
                    <td>Lorem Ipsum dolor sit</td>
                    <td>Lorem Ipsum dolor sit</td>
                    <td>Lorem Ipsum dolor sit</td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
<?php }