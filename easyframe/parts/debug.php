<?php

function ef_debug() {
	if(EF_DEBUG == true || ef_is_local()) { ?>
		<div class="debug__notices"></div>
		<?php if(is_user_logged_in()) { 
		    $url = wp_logout_url();
		    $label = __('Odhlásit se', EF_THEME);
		} else {
			if(is_singular()) {
				$link = 'post.php?post=' . get_the_ID() . '&action=edit';
				if(!empty($GLOBALS['sitepress'])) {
					$link .= '&lang=' . ICL_LANGUAGE_CODE;
				}
				$url = admin_url($link);
			} else {
				$url = wp_login_url('http://' . $_SERVER['HTTP_HOST']. $_SERVER['REQUEST_URI']);
			}
		    $label = __('Přihlásit se', EF_THEME);
		} ?>
		<a class="debug__login" href="<?php echo $url; ?>"><?php echo $label; ?></a>		
	<?php }
}