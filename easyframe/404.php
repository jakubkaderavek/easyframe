<?php
get_header();
?>

<div id="post-0" class="post error404 not-found">
    <h1><?php _e('Nenalezeno', EF_THEME); ?></h1>
    <p><?php _e('Omlouváme se, ale stránka, kterou chcete zobrazit, neexistuje. Zkuste jí najít pomocí vyhledávání.', EF_THEME); ?></p>
    <?php get_search_form(); ?>
</div>

<script>
    document.getElementById('s') && document.getElementById('s').focus();
</script>
<?php get_footer(); ?>