<?php
get_header();
?>

<h1><?php single_cat_title('', true); ?></h1>
<?php
$category_description = category_description();
if (!empty($category_description)) {
    echo $category_description;
}
get_template_part('loops/category-content');

get_footer();