<?php
// header
?><!DOCTYPE html>
<html dir="ltr" <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script>
        document.documentElement.className = document.documentElement.className.replace("no-js", "js");
    </script>
    <?php wp_head(); ?>
    <script>
        !function (e) {
            var t = function (t, n) {
                "use strict";
                var o = e.document.getElementsByTagName("script")[0], r = e.document.createElement("script");
                return r.src = t, r.async = !0, o.parentNode.insertBefore(r, o), n && "function" == typeof n && (r.onload = n), r
            };
            "undefined" != typeof module ? module.exports = t : e.loadJS = t
        }("undefined" != typeof global ? global : this);
        loadJS("<?php echo EF_ASSETS_URL . 'js/script.' . EF_VERSION . '.min.js'; ?>", function () {
        });
    </script>
</head>
<body <?php body_class(); ?>>
<?php echo ef_debug(); ?>
<header class="header">
    <div class="container">
        <a class="header__go_to_content" href="#content"
           title="<?php esc_attr_e('Přejít na obsah', EF_THEME); ?>"><?php _e('Přejít na obsah', EF_THEME); ?></a>
        <a class="header__logo" href="<?php echo home_url(); ?>">
            <?php echo ef_get_image(EF_ASSETS_URL . '/images/logo.png', __('Logo', EF_THEME)); ?>
        </a>
        <div class="header__menu">
            <button id="header__menu__button" class="header__menu__button"><?php _e('Menu', EF_THEME); ?></button>
            <?php wp_nav_menu(array(
                'theme_location' => 'menu',
                'container' => 'nav',
                'container_id' => 'header__menu__inner',
                'container_class' => 'header__menu__inner',
            )); ?>
        </div>
    </div>
</header>
<div class="container">
    <div id="content" class="content">
