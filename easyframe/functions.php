<?php
// functions

// constants

define('EF_THEME_DIR', get_template_directory() . '/');
define('EF_ASSETS_URL', get_template_directory_uri() . '/assets/');
define('EF_ASSETS_DIR', EF_THEME_DIR. 'assets/');
define('EF_VERSION', '100');
define('EF_PICTURE', true);
define('EF_RETINA', false);
define('EF_THEME', 'easyframe');
define('EF_DEBUG', true);
define('FRONT_PAGE', get_option('page_on_front'));
define('POST_PAGE', get_option('page_for_posts'));

require_once EF_THEME_DIR .'inc/admin-menu.php';
require_once EF_THEME_DIR .'inc/dummy-typo.php';
require_once EF_THEME_DIR .'inc/editor.php';
require_once EF_THEME_DIR .'inc/forms.php';
require_once EF_THEME_DIR .'inc/images.php';
require_once EF_THEME_DIR .'inc/post-types.php';
require_once EF_THEME_DIR .'inc/theme-functions.php';
require_once EF_THEME_DIR .'inc/users.php';
require_once EF_THEME_DIR .'libs/screenwidth/screenwidth.php';
require_once EF_THEME_DIR .'plugins/wordpress-seo.php';
require_once EF_THEME_DIR .'parts/debug.php';