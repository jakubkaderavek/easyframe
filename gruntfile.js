module.exports = function (grunt) {
    grunt.initConfig({
        path: 'easyframe',
        pkg: grunt.file.readJSON('package.json'),
        less: {
            development: {
                options: {
                    compress: true,
                    plugins: [
                        new (require('less-plugin-autoprefix'))({browsers: ["last 2 versions"]}),
                        new (require('less-plugin-clean-css'))()
                    ],
                },
                files: {
                    '<%= path %>/assets/css/style.min.css': '<%= path %>/less/style.less'
                }
            }
        },
        jshint: {
            beforeconcat: ['<%= path %>/libs/screenwidth/screenwidth.js', '<%= path %>/libs/screenwidth-same-height/same-height.js', '<%= path %>/inc/*.js', '<%= path %>/loops/*.js', '<%= path %>/shortcodes/*.js', '<%= path %>/parts/*.js'],
        },
        concat: {
            options: {
                separator: '; '
            },
            main: {
                src: ['<%= path %>/libs/*.js', '<%= path %>/libs/**/*.js', '<%= path %>/inc/*.js', '<%= path %>/*.js', '<%= path %>/loops/*.js', '<%= path %>/shortcodes/*.js', '<%= path %>/parts/*.js', '<%= path %>/external/*.js'],
                dest: '<%= path %>/assets/js/script.min.js'
            },
        },
        uglify: {
            my_target: {
                files: {
                    '<%= path %>/assets/js/script.min.js': '<%= path %>/assets/js/script.min.js',
                }
            }
        },
        criticalcss: {
            homepage: {
                options: {
                    url: 'http://www.ef.loc',
                    outputfile: '<%= path %>/assets/css/critical-home.min.css',
                    filename: '<%= path %>/assets/css/style.min.css',
                    width: 1200,
                    height: 900,
                    buffer: 800 * 1024,
                    ignoreConsole: false

                }
            },
                /*
            other: {
                options: {
                    url: 'http://www.ef.loc/other',
                    outputfile: '<%= path %>/assets/css/critical.min.css',
                    filename: '<%= path %>/assets/css/style.min.css',
                    width: 1200,
                    height: 900,
                    buffer: 800 * 1024,
                    ignoreConsole: false,
                }
            }
            */
        },
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    '<%= path %>/assets/css/critical-home.min.css': ['<%= path %>/assets/css/critical-home.min.css'],
                  //  '<%= path %>/assets/css/critical.min.css': ['<%= path %>/assets/css/critical.min.css']
                }
            }
        },
        focus: {
            dev: {
                exclude: ['styles_critical', 'scripts_uglify']
            },
            prod: {
                exclude: ['styles', 'scripts']
            },
        },
        watch: {
            styles: {
                files: ['<%= path %>{/*,/**/*,/**/**/*,/**/**/**/*}.less', '<%= path %>{/*,/**/*,/**/**/*,/**/**/**/*}.{svg,jpg,png}'],
                tasks: ['less'],
            },
            styles_critical: {
                files: ['<%= path %>{/*,/**/*,/**/**/*,/**/**/**/*}.less', '<%= path %>{/*,/**/*,/**/**/*,/**/**/**/*}.{svg,jpg,png}'],
                tasks: ['less', 'criticalcss', 'cssmin'],
            },
            scripts: {
                files: ['<%= path %>/js/*.js', '<%= path %>/*.js', '<%= path %>/inc/*.js', '<%= path %>/elements/*.js', '<%= path %>/loops/*.js', '<%= path %>/shortcodes/*.js', '<%= path %>/parts/*.js', '<%= path %>/external/*.js'],
                tasks: ['jshint', 'concat'],
            },
            scripts_uglify: {
                files: ['<%= path %>/js/*.js', '<%= path %>/*.js', '<%= path %>/inc/*.js', '<%= path %>/elements/*.js', '<%= path %>/loops/*.js', '<%= path %>/shortcodes/*.js', '<%= path %>/parts/*.js', '<%= path %>/external/*.js'],
                tasks: ['jshint', 'concat', 'uglify'],
            },
            php_js_css: {
                files: ['<%= path %>{/*,/**/*,/**/**/*,/**/**/**/*}.php', '<%= path %>/assets/js/*.js', '<%= path %>/assets/css/*.css'],
                options: {
                    livereload: true,
                }
            },

        },
    });
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-criticalcss');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-focus');

    grunt.registerTask('default', ['less', 'jshint', 'concat', 'focus:dev']);
    grunt.registerTask('prod', ['less', 'criticalcss', 'cssmin', 'jshint', 'concat', 'uglify', 'focus:prod']);

};